'use strict';
var orders_bot = require('./dxp-orders-bot');
const account = 'you bot account with initial funds with both assets';
const active_key_wif = 'the active private key in wif format starts with 5';
const strategy = {
    base_asset_symbol: 'CNY', 
    quote_asset_symbol: 'DXP',
    price_diff: 0.1,
    ratio_diff: 0.04,
    min_ratio: 0.25,
    max_ratio: 0.75,
    price_adjust_ratio: 2
}
orders_bot.simple_bot(account, active_key_wif, strategy);